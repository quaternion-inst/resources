# Algorithms
## Free Online
- **[Algorithms, 4th Edition](https://algs4.cs.princeton.edu/home/)** by [Robert Sedgewick](https://www.cs.princeton.edu/~rs/)) and [Kevin Wayne](https://www.cs.princeton.edu/~wayne/contact/)
- **[Algorithms](http://jeffe.cs.illinois.edu/teaching/algorithms/)** by [Jeff Erickson](http://jeffe.cs.illinois.edu/)
- **[Algorithms](https://www.khanacademy.org/computing/computer-science/algorithms/)** by [Khan Academy](https://www.khanacademy.org/)
- **[CMU 15-451 Lecture Notes](https://www.cs.cmu.edu/~15451-s20/schedule.html)** 
- **[CSE373 - Analysis of Algorithms Video Lectures](https://www.youtube.com/playlist?list=PLOtl7M3yp-DX32N0fVIyvn7ipWKNGmwpp)** by [Steven Skiena](https://www.cs.stonybrook.edu/people/faculty/StevenSkiena)
- **[Fundamentals of Algorithms](https://www.geeksforgeeks.org/fundamentals-of-algorithms/)**

### Supplementary
- **[Data Structures & Algorithms I Used Working at Tech Companies](https://blog.pragmaticengineer.com/data-structures-and-algorithms-i-actually-used-day-to-day/)** by [Gergely Orosz](https://blog.pragmaticengineer.com/)
- **[Project Nayuki](https://www.nayuki.io/category/programming)**
- **[The Arcane Algorithm Archive](https://www.algorithm-archive.org/)**
- **[Visualizing Algorithms](https://bost.ocks.org/mike/algorithms/)** by [Mike Bostock](https://bost.ocks.org/mike/)

### Practice
- **[Project Euler](https://projecteuler.net/)**

## Physical
- **[Grokking Algorithms](https://www.manning.com/books/grokking-algorithms)** by Adita Bhargava
- **[Introduction to Algorithms](https://mitpress.mit.edu/books/introduction-algorithms-second-edition)** by [Thomas Cormen](https://www.cs.dartmouth.edu/~thc/), [Charles Leserson](http://people.csail.mit.edu/cel/), [Ronald Rivest](http://people.csail.mit.edu/rivest/), and [Clifford Stein](http://www.columbia.edu/~cs2035/)
- **[The Algorithm Design Manual](https://www.springer.com/us/book/9783030542559)** by [Steven Skiena](https://www.cs.stonybrook.edu/people/faculty/StevenSkiena)
