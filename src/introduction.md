[![Quaternion Institute](/branding/logo_text.svg)](https://quaternion.institute)

# Introduction
Welcome to the Quaternion Institute's list of learning resources.
Here lies a collection of readily available free online textbooks, articles, courses, etc. as well as physical books.
Resources are sorted by academic field and subject.
Any resources that are not text-based will be labelled as such.
If you are looking for something specific, try the search feature, accessible with the hotkey `s`. 

## How is this Organized?
Each field of study is a chapter in the sidebar, more specific subjects are sections.
The chapter page for each field of study contains lists of other resource lists for the topic in case you don't find what you are looking for.
On each subject page, resources are sorted alphabetically by given title.

## What is Included?
### Online Resources
Online resources must be free in their entirety with all learning material freely accessible.
Free trials, paid course demos, and anything else that asks for money are not allowed.
The only exceptions are websites asking for donations or selling physical materials such as print editions.
Ideally resources should not require account creation or email sign-up.

### Physical Resources
Physical materials cost money, books and such on this list are acceptable, though they will be judged with higher standards.
Only high-quality and well-reguarded books will be added when there are many choices.
If a book has a nickname, it's a safe bet: e.g. "Stewart's", "Griffith's", "The Dragon Book".
Books _may not_ rely on or refer heavily to restricted-access online materials (no access codes!).
Links should go to the author or publisher's site instead of a retailer's when possible.

## How can I Contribute?
See this list on [GitLab](https://gitlab.com/quaternion-inst/resources) if you are interested in contributing.
If you would like to see something added, let us know or open an issue/merge request on GitLab.
For questions, hop on [Gitter](https://gitter.im/quaternion-inst/community) and ask away.

## How is this Moderated?
Community input is weighed heavily but final decisions will be made by Quaternion Institute staff.
Please contact us if you believe something should be removed from the list or would like to discuss a moderation decision.
