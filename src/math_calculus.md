# Calculus
## Free Online
- **[Calculus](https://ocw.mit.edu/resources/res-18-001-calculus-online-textbook-spring-2005/textbook/)** by [Gilbert Strang](http://www-math.mit.edu/~gs/)
- **[Calculus](https://www.whitman.edu/mathematics/california_calculus/calculus.pdf)** by [David Guichard](http://people.whitman.edu/~guichard/)
- **[Essence of Calculus Videos](https://www.youtube.com/playlist?list=PLZHQObOWTQDMsr9K-rj53DwVRMYO3t5Yr)** by [Grant Sanderson](https://www.3blue1brown.com/about)
- **[Vector Calculus](http://mecmath.net/)** by [Michael Corral](http://www.mecmath.net/)

## Physical
- **[Calculus: An Intuitive and Physical Approach](https://store.doverpublications.com/0486404536.html)** by [Morris Kline](https://en.wikipedia.org/wiki/Morris_Kline)
- **[Calculus](https://www.mathpop.com/)** by [Michael Spivak](https://en.wikipedia.org/wiki/Michael_Spivak)
