# Differential Equations
## Free Online
- **[MIT 18.03 Differential Equations](https://ocw.mit.edu/courses/mathematics/18-03-differential-equations-spring-2010/index.htm)**
- **[Notes on Diffy Qs: Differential Equations for Engineers](https://www.jirka.org/diffyqs/)** by [Jiří Lebl](https://www.jirka.org/)
- **[Paul's Online Notes](https://tutorial.math.lamar.edu/Classes/DE/DE.aspx)**
- **[Differential Equations Videos](https://www.youtube.com/playlist?list=PLZHQObOWTQDNPOjrT6KVlfJuKtYTftqH6)** by [Grant Sanderson](https://www.3blue1brown.com/about)

## Physical
- **[Ordinary Differential Equations](https://mitpress.mit.edu/books/ordinary-differential-equations)** by [Vladimir Arnold](http://www.pdmi.ras.ru/~arnsem/Arnold/arn-papers.html)
- **[Nonlinear Dynamics and Chaos](http://www.stevenstrogatz.com/books/nonlinear-dynamics-and-chaos-with-applications-to-physics-biology-chemistry-and-engineering)** by [Steven Strogatz](http://www.stevenstrogatz.com/)
