# Data Structures
## Free Online
- **[Data Structures](https://www.geeksforgeeks.org/data-structures/)**
- **[Functional Data Structures](https://cs.uwaterloo.ca/~plragde/flaneries/FDS/)** by [Prabhakar Ragde](https://cs.uwaterloo.ca/~plragde/)
- **[MIT 6.851: Advanced Data Structures](http://courses.csail.mit.edu/6.851/)** by [Erik Demaine](http://erikdemaine.org/)

### Supplementary
- **[The Periodic Table of Data Structures](https://stratos.seas.harvard.edu/publications/periodic-table-data-structures)** by [Stratos Idreos](https://stratos.seas.harvard.edu/), et al.

## Physical
- **[Purely Functional Data Structures](https://www.cambridge.org/ca/academic/subjects/computer-science/programming-languages-and-applied-logic/purely-functional-data-structures?format=PB&isbn=9780521663502)** by [Chris Okasaki](https://en.wikipedia.org/wiki/Chris_Okasaki)
