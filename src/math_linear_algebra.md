# Linear Algebra
## Free Online
- **[Beginning and Intermediate Algebra](http://www.wallace.ccfaculty.org/book/book.html)** by [Tyler Wallace](http://www.wallace.ccfaculty.org/)
- **[Essence of Linear Algebra Videos](https://www.youtube.com/watch?v=fNk_zzaMoSs&list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab)** by [Grant Sanderson](https://www.3blue1brown.com/about)
- **[Interactive Linear Algebra](https://textbooks.math.gatech.edu/ila/index.html)** by [Dan Margalit](http://people.math.gatech.edu/~dmargalit7/index.shtml) and [Joseph Rabinoff](https://services.math.duke.edu/~jdr/)
- **[Introduction to Applied Linear Algebra](http://vmls-book.stanford.edu/)** by [Stephen Boyd](https://web.stanford.edu/~boyd/) and [Lieven Vandenberghe](http://www.seas.ucla.edu/~vandenbe/)
- **[Linear Algebra Done Right Lectures](https://linear.axler.net/LADRvideos.html)** by [Sheldon Axler](https://axler.net/)
- **[Linear Algebra Done Wrong](http://www.math.brown.edu/streil/papers/LADW/LADW.html)** by [Sergei Treil](https://sites.google.com/a/brown.edu/sergei-treil-homepage/home)
- **[Linear Algebra](http://joshua.smcvt.edu/linearalgebra/)** by [Jim Hefferon](http://joshua.smcvt.edu/math/hefferon.html)
- **[MIT 18.06 Linear Algebra](https://ocw.mit.edu/courses/mathematics/18-06-linear-algebra-spring-2010/)** by [Gilbert Strang](http://www-math.mit.edu/~gs/)
- **[immersive linear algebra](http://immersivemath.com/ila/index.html)** by J. Ström, K. Åström and T. Akenine-Möller

## Physical
- **[Linear Algebra Done Right](https://link.springer.com/book/10.1007/978-3-319-11080-6)** by [Sheldon Axler](https://axler.net)
- **[Introduction to Linear Algebra](http://math.mit.edu/~gs/linearalgebra/)** by [Gilbert Strang](http://www-math.mit.edu/~gs/)
