# Abstract Algebra
## Free Online
- **[Abstract Algebra Lectures](https://www.youtube.com/playlist?list=PLelIK3uylPMGzHBuR3hLMHrYfMqWWsmx5)** by [Benedict Gross](http://people.math.harvard.edu/~gross/)
- **[Abstract Algebra On Line](http://www.math.niu.edu/~beachy/aaol/)** by [John A. Beachy](http://www.math.niu.edu/~beachy/)
- **[Abstract Algebra Videos](https://www.socratica.com/subject/abstract-algebra)** by [Socratica](https://www.socratica.com/)
- **[Abstract Algebra: Theory and Applications](http://abstract.ups.edu/)** by Tom Judson

## Physical
- **[Abstract Algebra](https://www.wiley.com/en-us/Abstract+Algebra%2C+3rd+Edition-p-9780471433347)** by [David Dummit](https://www.uvm.edu/cems/mathstat/profiles/david_s_dummit) and [Richard Foote](http://www.cems.uvm.edu/~rfoote/)
- **[A Book of Abstract Algebra](https://store.doverpublications.com/0486474178.html)** by [Charles Pinter](http://www.charlespinter.com/)
