# Programming
## Free Online
- **[Learn Vim (the Smart Way)](https://github.com/iggredible/Learn-Vim)** by [Igor Irianto](https://irian.to/)
- **[Learn You a Haskell for Great Good](http://learnyouahaskell.com/chapters)** by Miran Lipovača
- **[Pro Git](https://git-scm.com/book/en/v2)** by [Scott Chacon](http://scottchacon.com/about.html) and [Ben Straub](https://ben.straub.cc/)
- **[The Rust Programming Language](https://doc.rust-lang.org/book/)** by [Steve Klabnik](https://steveklabnik.com/) and [Carol Nichols](http://carol-nichols.com/)

### Supplementary
- **[Learn X in Y minutes](https://learnxinyminutes.com/)**

## Physical
- **[The Pragmatic Programmer](https://pragprog.com/titles/tpp20/the-pragmatic-programmer-20th-anniversary-edition/)** by [Dave Thomas](https://en.wikipedia.org/wiki/Dave_Thomas_(programmer)) and [Andrew Hunt](https://en.wikipedia.org/wiki/Andy_Hunt_(author))
