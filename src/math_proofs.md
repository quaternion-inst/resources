# Proofs
## Free Online
- **[Book of Proof](http://www.people.vcu.edu/%7Erhammack/BookOfProof/)** by [Richard Hammack](http://www.people.vcu.edu/~rhammack/)
- **[How To Write Proofs](http://zimmer.csufresno.edu/%7Elarryc/proofs/proofs.html)** by [Larry Cusick](http://zimmer.fresnostate.edu/~larryc/)
- **[Introduction to mathematical arguments](https://math.berkeley.edu/~hutching/teach/proofs.pdf)** by [Michael Hutchings](https://math.berkeley.edu/~hutching/)
- **[Steps to Proving a Theorem](http://math.cmu.edu/%7Ewgunther/problemsolving.pdf)** by [William Gunther](http://math.cmu.edu/~wgunther/)

## Reference
- **[ProofWiki](https://proofwiki.org/wiki/Main_Page)**

## Physical
- **[How To Prove It](https://www.cambridge.org/core/books/how-to-prove-it/6D2965D625C6836CD4A785A2C843B3DA)** by [Daniel Velleman](https://directory.amherst.edu/people/facstaff/djvelleman)
- **[How To Solve It](https://press.princeton.edu/books/paperback/9780691164076/how-to-solve-it)** by [George Polya](https://www.math.wichita.edu/history/men/polya.html)
