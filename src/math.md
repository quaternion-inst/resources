# Mathematics
The study of numbers and patterns.

## Lists of Resources
- [Chicago undergraduate mathematics bibliography](https://www.ocf.berkeley.edu/%7Eabhishek/chicmath.htm)
- [Open Content Mathematics Curriculum](http://linear.ups.edu/curriculum.html)
- [Real Not Complex Math Resources](https://realnotcomplex.com/)

## Learning Advice 
- **[Various Articles on Succeeding in Math](https://brownmath.com/stfa/)** by [Stan Brown](https://brownmath.com/)
