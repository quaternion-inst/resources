# Compilers
## Free Online
- **[Assemblers and Loaders](http://www.davidsalomon.name/assem.advertis/AssemAd.html)** by [David Salomon](http://www.davidsalomon.name/)
- **[CS 6120: Advanced Compilers: The Self-Guided Online Course](https://www.cs.cornell.edu/courses/cs6120/2020fa/self-guided/)** by [Adrian Sampson](https://www.cs.cornell.edu/%7Easampson/)
- **[Crafting Interpreters](https://craftinginterpreters.com/)** by [Bob Nystrom](http://journal.stuffwithstuff.com/)
- **[Stanford Class Notes](https://github.com/darshanime/notes/blob/master/compilers.org)**
- **[UCSD CSE 131: Compilers](https://cseweb.ucsd.edu/classes/sp17/cse131-a/index.html)** by [Joe Politz](https://jpolitz.github.io/)

## Supplementary
- **[An Intro to Compilers](https://nicoleorchard.com/blog/compilers)** by [Nicole Orchard](https://nicoleorchard.com/)

## Physical
- **[Compilers: Principles, Techniques, and Tools](https://suif.stanford.edu/dragonbook/)** *"The Dragon Book"* by [Alfred Aho](http://www1.cs.columbia.edu/~aho/), [Monica Lam](https://suif.stanford.edu/~lam/), [Ravi Sethi](https://www2.cs.arizona.edu/~rsethi/), [Jeffrey Ullman](http://infolab.stanford.edu/~ullman/)
