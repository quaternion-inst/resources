# Computer Science
The study of computational machinery and algorithmic processes.

## Lists of Resources
- [Computer Science](https://github.com/ossu/computer-science)
- [Free Programming Books](https://ebookfoundation.github.io/free-programming-books/)
- [OSDev.org Book List](https://wiki.osdev.org/Books)
- [Teach Yourself Computer Science](https://teachyourselfcs.com/)
- [The Open Source Computer Science Degree](https://github.com/ForrestKnight/open-source-cs)
