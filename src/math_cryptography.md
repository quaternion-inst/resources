# Cryptography
## Free Online
- **[A Graduate Course in Applied Cryptography](https://toc.cryptobook.us/)** by [Dan Boneh](https://crypto.stanford.edu/~dabo/) and [Victor Shoup](https://shoup.net/)
- **[Cryptography: An Introduction](https://www.cs.umd.edu/~waa/414-F11/IntroToCrypto.pdf)** by [Nigel Smart](https://homes.esat.kuleuven.be/~nsmart/)

## Physical
- **[An Introduction to Mathematical Cryptography](https://www.math.brown.edu/johsilve/MathCryptoHome.html)** by [Jeffrey Hoffstein](https://www.brown.edu/academics/math/people/jeffrey-hoffstein), [Jill Pipher](https://www.math.brown.edu/jpipher/), and [Joseph Silverman](http://www.math.brown.edu/johsilve/)
- **[Cryptography Engineering](https://www.schneier.com/books/cryptography-engineering)** by [Niels Ferguson](https://en.wikipedia.org/wiki/Niels_Ferguson), [Bruce Schneier](https://www.hks.harvard.edu/faculty/bruce-schneier), and [Tadayoshi Kohno](https://homes.cs.washington.edu/~yoshi/)
